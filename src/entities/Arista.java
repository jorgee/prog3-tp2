package entities;

public class Arista {
	
	protected Vertice vertice1;
	protected Vertice vertice2;
	protected int peso;
	
	public Arista(Vertice vertice1, Vertice vertice2, int peso) {
		this.vertice1 = vertice1;
		this.vertice2 = vertice2;
		this.peso = peso;
	}

	public Vertice getVertice1() {
		return vertice1;
	}

	public Vertice getVertice2() {
		return vertice2;
	}

	public int getPeso() {
		return peso;
	}
	
	public void setPeso(int peso ) {
		this.peso = peso;
	}
	
	public int hashCode() {
		return vertice1.hashCode() + vertice2.hashCode();
	}
	
	@Override
	public boolean equals(Object object) {
		if (object instanceof Arista) {
			Arista arista = (Arista) object;
			if (arista == this) {
				return true;
			}
			if ((arista.getVertice1() == this.vertice1 && arista.getVertice2() == this.vertice2)
				|| (arista.getVertice2() == this.vertice1 && arista.getVertice1() == this.vertice2)) {
				return true;
			}
		}
		return false;
	}
}
