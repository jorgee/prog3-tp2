package entities;

import java.util.HashMap;
import java.util.HashSet;

public class Grafo {

	private HashSet<Vertice> vertices;
	private HashSet<Arista> aristas;
	protected HashMap<Integer, HashSet<Arista>> vecinos;

	public Grafo()
	{
		this.vertices = new HashSet<Vertice>();
		this.aristas = new HashSet<Arista>();
		this.vecinos = new HashMap<Integer, HashSet<Arista>>();
	}
	
	public int getVerticesQuantity()
	{
		return this.vertices.size();
	}

	public int getAristasQuantity()
	{
		return this.aristas.size();
	}
	
	public HashSet<Arista> getAristas() {
		return this.aristas;
	}
	
	public void agregarArista(Arista arista) throws Exception
	{
		if (!(this.vertices.contains(arista.getVertice1()) && this.vertices.contains(arista.getVertice1()))) {
			throw new Exception("La arista contiene vértices que no pertenecen al grafo.");
		}
		this.aristas.add(arista);
		this.vecinos.get(arista.getVertice1().hashCode()).add(arista);
		this.vecinos.get(arista.getVertice2().hashCode()).add(arista);
	}
	
	public void agregarVertice(Vertice vertice) {
		if (!this.vertices.contains(vertice)) {
			this.vertices.add(vertice);
			this.vecinos.put(vertice.hashCode(), new HashSet<Arista>());
		}
	}
	
	public boolean hasArista(Arista arista) {
		return this.aristas.contains(arista);
	}
	
	public boolean hasVertice(Vertice vertice) {
		return this.vertices.contains(vertice);
	}

	public void eliminarArista(Arista arista)
	{
		this.aristas.remove(arista);
		this.vecinos.get(arista.getVertice1().hashCode()).remove(arista);
		this.vecinos.get(arista.getVertice2().hashCode()).remove(arista);
	}
	
	public void eliminarVertice(Vertice vertice)
	{
		this.vertices.remove(vertice);
		for (Arista arista : this.obtenerAristasIncidentes(vertice)) {
			this.eliminarArista(arista);
		}
	}
	
	public HashSet<Arista> obtenerAristasIncidentes(Vertice vertice) {
		return this.vecinos.get(vertice.hashCode());
	}
}
