package entities;

public class AgentNetworkVertex extends Vertice {

	Agent agent;
	
	public AgentNetworkVertex(String name, Agent agent) {
		super(name);
		this.agent = agent;
	}

	public Agent getAgent() {
		return this.agent;
	}
	
	public int hashCode() {
		return this.agent.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof AgentNetworkVertex) {
			AgentNetworkVertex agentNetworkVertex = (AgentNetworkVertex) o;
			return agentNetworkVertex.getAgent() == this.getAgent();
		}
		return false;
	}
}
