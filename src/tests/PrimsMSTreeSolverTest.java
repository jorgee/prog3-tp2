package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Random;

import org.junit.jupiter.api.Test;

import entities.Arista;
import entities.Grafo;
import entities.Vertice;
import services.PrimsMSTreeSolver;

public class PrimsMSTreeSolverTest {
	
	@Test
	public void testEmptyGraphResultsInEmptyGraph() {
		PrimsMSTreeSolver solver = new PrimsMSTreeSolver();
		Grafo graph = new Grafo();
		assertEquals(0, solver.generate(graph).getAristas().size());
	}
	
	@Test
	public void testMSTreeForTwoVertices() throws Exception {
		PrimsMSTreeSolver solver = new PrimsMSTreeSolver();
		Grafo graph = new Grafo();
		Vertice vertice1 = new Vertice("Vertice 1");
        Vertice vertice2 = new Vertice("Vertice 2");
        Arista arista = new Arista(vertice1, vertice2, 10);
        graph.agregarVertice(vertice1);
        graph.agregarVertice(vertice2);
        graph.agregarArista(arista);
        
		assertEquals(1, solver.generate(graph).getAristas().size());
	}
	
	/**
	 * Por caracterización de árbol, un árbol de n vértices tiene n-1 aristas.
	 * @throws Exception
	 */
	@Test
	public void testMSTreeForRandomVerticesDoesntGenerateCycles() throws Exception {
		PrimsMSTreeSolver solver = new PrimsMSTreeSolver();
		Grafo graph = new Grafo();
		Random randomGenerator = new Random();
		int min = 1;
		int max = 50;
		int randomNumber = randomGenerator.nextInt((max - min) + 1) + min;
		
		ArrayList<Vertice> vertices = new ArrayList<Vertice>();
		// Agregar vértices al grafo
		for (int i = 0; i < randomNumber; i++) {
			Vertice vertice1 = new Vertice("Vertice " + i);
			vertices.add(vertice1);
	        graph.agregarVertice(vertice1);
		}
		// Generar grafo completo
		for (int i = 0; i < vertices.size(); i++) {
			for (int j = i+1; j < vertices.size(); j++) {
				Arista arista = new Arista(vertices.get(i), vertices.get(j), randomGenerator.nextInt((max - min) + 1) + min);
				graph.agregarArista(arista);
			}
		}
		assertEquals(randomNumber - 1, solver.generate(graph).getAristas().size());
	}
}
