package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import entities.Arista;
import entities.Grafo;
import entities.Vertice;

public class GrafoTest {

    @Test
    public final void testVerticeIsAdded() {
        Grafo grafo = new Grafo();
        Vertice vertice1 = new Vertice("Vertice 1");
        
        grafo.agregarVertice(vertice1);
        
        assertEquals(1, grafo.getVerticesQuantity());
        assertTrue(grafo.hasVertice(vertice1));
    }
    
    @Test(expected = Exception.class)
    public final void testAristaCannotBeAddedIfMissingOneVertexInGraph() throws Exception {
    	Grafo grafo = new Grafo();
        Vertice vertice1 = new Vertice("Vertice 1");
        Vertice vertice2 = new Vertice("Vertice 2");
        Arista arista = new Arista(vertice1, vertice2, 10);

        grafo.agregarVertice(vertice1);
        grafo.agregarArista(arista);
    }
    
    @Test
    public final void testAristaIsAdded() throws Exception {
    	Grafo grafo = new Grafo();
        Vertice vertice1 = new Vertice("Vertice 1");
        Vertice vertice2 = new Vertice("Vertice 2");
        Arista arista = new Arista(vertice1, vertice2, 10);

        grafo.agregarVertice(vertice1);
        grafo.agregarVertice(vertice2);
        grafo.agregarArista(arista);
        
        assertEquals(1, grafo.getAristasQuantity());
        assertTrue(grafo.hasArista(arista));
    }
    
    @Test
    public final void testAristaIsRemoved() throws Exception {
    	Grafo grafo = new Grafo();
        Vertice vertice1 = new Vertice("Vertice 1");
        Vertice vertice2 = new Vertice("Vertice 2");
        Arista arista = new Arista(vertice1, vertice2, 10);
        grafo.agregarVertice(vertice1);
        grafo.agregarVertice(vertice2);
        grafo.agregarArista(arista);
        
        grafo.eliminarArista(arista);
        assertFalse(grafo.hasArista(arista));
        assertEquals(0, grafo.getAristasQuantity());
    }
    
    @Test
    public final void testAristaWithSameVerticesIsNotAddedTwice() throws Exception {
    	Grafo grafo = new Grafo();
        Vertice vertice1 = new Vertice("Vertice 1");
        Vertice vertice2 = new Vertice("Vertice 2");
        grafo.agregarVertice(vertice1);
        grafo.agregarVertice(vertice2);
        Arista arista = new Arista(vertice1, vertice2, 10);
        Arista arista2 = new Arista(vertice2, vertice1, 10);
        grafo.agregarArista(arista);
        grafo.agregarArista(arista);
        
        assertEquals(1, grafo.getAristasQuantity());
        assertTrue(grafo.hasArista(arista));
        assertTrue(grafo.hasArista(arista2));
    }
    
    @Test
    public final void testVecinosAreCorrectWhenAddingAristas() throws Exception {
    	Grafo grafo = new Grafo();
    	Vertice vertice1 = new Vertice("Vertice 1");
        Vertice vertice2 = new Vertice("Vertice 2");
        Vertice vertice3 = new Vertice("Vertice 3");
        Arista arista = new Arista(vertice1, vertice2, 10);
        Arista arista2 = new Arista(vertice1, vertice3, 10);

        grafo.agregarVertice(vertice1);
        grafo.agregarVertice(vertice2);
        grafo.agregarVertice(vertice3);
        
        grafo.agregarArista(arista);
        grafo.agregarArista(arista2);
        
        assertEquals(2, grafo.obtenerAristasIncidentes(vertice1).size());
        assertEquals(1, grafo.obtenerAristasIncidentes(vertice2).size());
        assertEquals(1, grafo.obtenerAristasIncidentes(vertice3).size());
        assertTrue(grafo.obtenerAristasIncidentes(vertice1).contains(arista));
    }
    
    @Test
    public final void testVecinosAreCorrectWhenRemovingAristas() throws Exception {
    	Grafo grafo = new Grafo();
        
    	Vertice vertice1 = new Vertice("Vertice 1");
        Vertice vertice2 = new Vertice("Vertice 2");
        Vertice vertice3 = new Vertice("Vertice 3");
        Arista arista = new Arista(vertice1, vertice2, 10);
        Arista arista2 = new Arista(vertice1, vertice3, 10);

        grafo.agregarVertice(vertice1);
        grafo.agregarVertice(vertice2);
        grafo.agregarVertice(vertice3);
        
        grafo.agregarArista(arista);
        grafo.agregarArista(arista2);
        
        grafo.eliminarArista(arista2);
        assertEquals(1, grafo.obtenerAristasIncidentes(vertice1).size());
        assertEquals(0, grafo.obtenerAristasIncidentes(vertice3).size());
    }
    
    @Test
    public final void testVerticesRemainWhenRemovingAristas() throws Exception {
    	Grafo grafo = new Grafo();
        
    	Vertice vertice1 = new Vertice("Vertice 1");
        Vertice vertice2 = new Vertice("Vertice 2");
        Vertice vertice3 = new Vertice("Vertice 3");
        Arista arista = new Arista(vertice1, vertice2, 10);
        Arista arista2 = new Arista(vertice1, vertice3, 10);

        grafo.agregarVertice(vertice1);
        grafo.agregarVertice(vertice2);
        grafo.agregarVertice(vertice3);
        
        grafo.agregarArista(arista);
        grafo.agregarArista(arista2);
        grafo.eliminarArista(arista2);
        grafo.eliminarArista(arista);
        
        assertTrue(grafo.hasVertice(vertice1));
        assertTrue(grafo.hasVertice(vertice2));
        assertTrue(grafo.hasVertice(vertice3));
    }
}
