package controllers;

import org.openstreetmap.gui.jmapviewer.Coordinate;

import entities.Agent;
import entities.Grafo;
import interfaces.MSTreeSolverInterface;
import main.App;
import services.AgentManagerService;
import services.PrimsMSTreeSolver;

public class MainController extends Controller {
	
	protected AgentManagerService agentManagerService;
	
	public MainController(App app, AgentManagerService agentRegistrationService) {
		super(app);
		this.agentManagerService = agentRegistrationService;
	}

	public void actionMain() {
		try {
			this.render("Main");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void actionRegisterAgent(Agent agent) {
		this.agentManagerService.registerAgent(agent);
	}
	
	public Agent actionGetAgentInCoordinate(Coordinate coordinate) {
		return this.agentManagerService.getAgentBy(coordinate);
	}

	public void actionRegisterAgentLink(Agent agent1, Agent agent2, int distance) throws Exception {
		this.agentManagerService.registerAgentLink(agent1, agent2, distance);		
	}
	
	public void actionResetAgentNetwork() {
		this.agentManagerService.resetAgentNetwork();
	}
	
	public Grafo actionGenerateAgentNetwork() {
		try {
			return this.agentManagerService.generateAgentNetwork();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Grafo actionGenerateMSTreeForAgentNetwork(Grafo network) {
		MSTreeSolverInterface algorithm = new PrimsMSTreeSolver();
		return this.agentManagerService.generateAgentNetworkMinimumSpanningTree(algorithm, network);
	}
}
