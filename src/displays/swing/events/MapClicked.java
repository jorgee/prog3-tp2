package displays.swing.events;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.interfaces.ICoordinate;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

import controllers.Controller;
import controllers.MainController;
import displays.swing.Swing;
import displays.swing.views.Main;
import entities.Agent;

public class MapClicked extends MouseAdapter {
	
	private JMapViewer map;
	Swing swingDisplay;
	Main view;
	
	public MapClicked(JMapViewer map, Main view, Swing swingDisplay) {
		this.map = map;
		this.swingDisplay = swingDisplay;
		this.view = view;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON1) {
            map.getAttribution().handleAttribution(e.getPoint(), true);
            ICoordinate position = map.getPosition(e.getPoint());
 
            String agentName = "Agent " + map.getMapMarkerList().size();
            Coordinate coordinate = new Coordinate(position.getLat(), position.getLon());
            MapMarker marker = new MapMarkerDot(agentName, coordinate);
            map.addMapMarker(marker);
            Controller controller = this.swingDisplay.getController();
            if (controller instanceof MainController) {
            	((MainController) controller).actionRegisterAgent(new Agent(agentName, position.getLat(), position.getLon()));
            }
            this.view.getButtonsInView().get("btnGenerarEnlaces").setEnabled(true);
            this.view.getButtonsInView().get("btnGenerarArbol").setEnabled(false);
        }
	}
}