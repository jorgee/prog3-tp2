package displays.swing.events;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.OsmMercator;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

import controllers.Controller;
import controllers.MainController;
import displays.swing.Swing;
import displays.swing.views.Main;
import entities.Agent;

public class GenerateNetworkButtonClicked implements ActionListener {

	JMapViewer map;
	Swing swingDisplay;
	Main view;
	
	public GenerateNetworkButtonClicked(JMapViewer map, Main view, Swing swingDisplay) {
		this.map = map;
		this.swingDisplay = swingDisplay;
		this.view = view;
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		Controller controller = this.swingDisplay.getController();
        if (controller instanceof MainController) {
        	((MainController) controller).actionResetAgentNetwork();
        }
		List<MapMarker> agentsMapMarkers = this.map.getMapMarkerList();
		this.map.removeAllMapPolygons();
		OsmMercator mercator = new OsmMercator();
		
		for (int i = 0; i < agentsMapMarkers.size(); i++) {
			MapMarker agent1 = agentsMapMarkers.get(i);
			Coordinate c1 = agent1.getCoordinate();
			for (int j = i+1; j < agentsMapMarkers.size(); j++) {
				MapMarker agent2 = agentsMapMarkers.get(j);
				Coordinate c2 = agent2.getCoordinate();
		        double distanceInMeters = mercator.getDistance(c1.getLat(), c1.getLon(), c2.getLat(), c2.getLon());
		        int distanceInKms = (int) (distanceInMeters / 1000);
				MapPolygonImpl line = this.createLine(c1, c2, Integer.toString(distanceInKms));
				this.map.addMapPolygon(line);
				try {
					this.registerAgentNetworkConnection(agent1, agent2, distanceInKms);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
        this.view.getButtonsInView().get("btnGenerarArbol").setEnabled(true);
	}
	
	protected void registerAgentNetworkConnection(MapMarker agent1marker, MapMarker agent2marker, int distance) throws Exception {
		Controller controller = this.swingDisplay.getController();
        if (controller instanceof MainController) {
        	MainController mainController = (MainController) controller;
        	Agent agent1 = mainController.actionGetAgentInCoordinate(agent1marker.getCoordinate());
    		Agent agent2 = mainController.actionGetAgentInCoordinate(agent2marker.getCoordinate());
    		mainController.actionRegisterAgentLink(agent1, agent2, distance);
        }
	}

	protected MapPolygonImpl createLine(Coordinate c1, Coordinate c2, String name) {
		List<Coordinate> route = new ArrayList<Coordinate>(Arrays.asList(c1, c2, c2));
		MapPolygonImpl poly = new MapPolygonImpl(route);
		// Cambiar tamaño de fuente. No pude encontrar la forma de cambiar el color.
		Font font = poly.getFont();
		String fontName = font.getName();
		Font font2 = new Font(fontName, font.getStyle(), 18);
		poly.setFont(font2);
		poly.setName(name);
		Color color = new Color(255, 255, 255);
		poly.setColor(color);
		return poly;
	}
}
