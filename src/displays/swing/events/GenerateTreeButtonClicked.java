package displays.swing.events;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;

import controllers.Controller;
import controllers.MainController;
import displays.swing.Swing;
import displays.swing.views.Main;
import entities.AgentNetworkVertex;
import entities.Arista;
import entities.Grafo;

public class GenerateTreeButtonClicked implements ActionListener {

	JMapViewer map;
	Swing swingDisplay;
	Main view;
	
	public GenerateTreeButtonClicked(JMapViewer map, Main view, Swing swingDisplay) {
		this.map = map;
		this.swingDisplay = swingDisplay;
		this.view = view;
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		Grafo agentNetwork = null;
		Controller controller = this.swingDisplay.getController();
        if (controller instanceof MainController) {
        	MainController mainController = (MainController) controller;
        	agentNetwork = mainController.actionGenerateAgentNetwork();
        	this.map.removeAllMapPolygons();
    		
    		if (agentNetwork != null) {
    			Grafo mstree = mainController.actionGenerateMSTreeForAgentNetwork(agentNetwork);
    			for (Arista arista : mstree.getAristas()) {
    				AgentNetworkVertex agent1 = (AgentNetworkVertex) arista.getVertice1();
    				AgentNetworkVertex agent2 = (AgentNetworkVertex) arista.getVertice2();
    				Coordinate c1 = new Coordinate(agent1.getAgent().getLat(), agent1.getAgent().getLon());
    				Coordinate c2 = new Coordinate(agent2.getAgent().getLat(), agent2.getAgent().getLon());
    		        int distanceInKms = arista.getPeso();
    				MapPolygonImpl line = this.createLine(c1, c2, Integer.toString(distanceInKms));
    				this.map.addMapPolygon(line);
    			}
    		}
        }
	}

	protected MapPolygonImpl createLine(Coordinate c1, Coordinate c2, String name) {
		List<Coordinate> route = new ArrayList<Coordinate>(Arrays.asList(c1, c2, c2));
		MapPolygonImpl poly = new MapPolygonImpl(route);
		// Cambiar tamaño de fuente. No pude encontrar la forma de cambiar el color.
		Font font = poly.getFont();
		String fontName = font.getName();
		Font font2 = new Font(fontName, font.getStyle(), 18);
		poly.setFont(font2);
		poly.setName(name);
		Color color = new Color(255, 0, 255);
		poly.setColor(color);
		return poly;
	}
}
