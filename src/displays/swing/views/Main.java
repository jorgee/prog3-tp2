package displays.swing.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Point;
import java.util.HashMap;

import javax.swing.JPanel;

import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.tilesources.BingAerialTileSource;

import displays.swing.Swing;
import displays.swing.SwingViewInterface;
import displays.swing.events.GenerateNetworkButtonClicked;
import displays.swing.events.GenerateTreeButtonClicked;
import displays.swing.events.MapClicked;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.Dimension;

public class Main implements SwingViewInterface {
	
	private JPanel panel;
	private Swing swingDisplay;
	private HashMap<String, JButton> buttonsInView;
	
	public Main(Swing swingDisplay) {
		this.swingDisplay = swingDisplay;
		this.buttonsInView = new HashMap<String, JButton>();
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	public void render() {

		/**
		 * Clase custom para poder hacer override de setZoom, para que no acepte un nivel menor que 3
		 * ya que el mapa carga completo en una fracción del container de la app y queda f
		 */
	    class CustomMap extends JMapViewer {
	        /**
			 * 
			 */
			private static final long serialVersionUID = 8229698174307043365L;

			@Override
	        public void setZoom(int zoom, Point mapPoint) {
	            if (zoom >= 3)
	                super.setZoom(zoom, mapPoint);
	        }
	    }
		JMapViewer map = new CustomMap();
		map.addMouseListener(new MapClicked(map, this, this.swingDisplay));
        map.setTileSource(new BingAerialTileSource());
        map.setZoomControlsVisible(false);
        map.setForeground(Color.WHITE);
        JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout(0, 0));
        panel.setBorder(new LineBorder(new Color(0, 0, 0)));
        panel.add(map, BorderLayout.CENTER);
		this.panel = panel;

		this.swingDisplay.getContainer().getContentPane().add(this.getPanel(), "mainViewPanel");
		
		JPanel panelSur = new JPanel();
		panelSur.setPreferredSize(new Dimension(10, 80));
		panelSur.setBackground(Color.ORANGE);
		panel.add(panelSur, BorderLayout.SOUTH);
		panelSur.setLayout(null);
		
		JButton btnGenerarEnlaces = new JButton("Generar enlaces");
		btnGenerarEnlaces.setEnabled(false);
		btnGenerarEnlaces.setBounds(34, 28, 161, 25);
		btnGenerarEnlaces.addActionListener(new GenerateNetworkButtonClicked(map, this, this.swingDisplay));
		this.buttonsInView.put("btnGenerarEnlaces", btnGenerarEnlaces);
		
		JButton btnGenerarArbol = new JButton("Generar árbol de comunicación");
		btnGenerarArbol.setEnabled(false);
		btnGenerarArbol.addActionListener(new GenerateTreeButtonClicked(map, this, this.swingDisplay));
		btnGenerarArbol.setBounds(221, 28, 282, 25);
		this.buttonsInView.put("btnGenerarArbol", btnGenerarArbol);
		
		panelSur.add(btnGenerarEnlaces);
		panelSur.add(btnGenerarArbol);
	}

	@Override
	public Component getPanel() {
		return this.panel;
	}

	public HashMap<String, JButton> getButtonsInView() {
		return buttonsInView;
	}
}
