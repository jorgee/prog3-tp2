package services;

import java.util.HashSet;

import entities.Arista;
import entities.Grafo;
import entities.Vertice;
import interfaces.MSTreeSolverInterface;

public class PrimsMSTreeSolver implements MSTreeSolverInterface {
	
	HashSet<Vertice> visitedVertices;
	HashSet<Arista> savedEdges;

	@Override
	public Grafo generate(Grafo graph) {
		Grafo newGraph = new Grafo();
		if (graph.getVerticesQuantity() == 1) {
			return newGraph;
		}
		this.visitedVertices = new HashSet<Vertice>();
		this.savedEdges = new HashSet<Arista>();
		
		HashSet<Arista> edges = graph.getAristas();
		Vertice vertex = null;
		// Fill first element then break to avoid asking the same conditional.
		for (Arista arista : edges) {
			if (vertex == null) { 
				vertex = arista.getVertice1();
				this.visitedVertices.add(vertex);
				break;
			}
		}
		int graphVerticesQty = graph.getVerticesQuantity();
		while (this.visitedVertices.size() < graphVerticesQty) {
			Arista lowestWeigthEdge = null;
			for (Vertice vertexToSearch : this.visitedVertices) {
				edges = graph.obtenerAristasIncidentes(vertexToSearch);
				Arista currentLowestWeigthEdge = getLowestWeightEdgeWithUnvisitedVertex(edges, vertex);
				if (lowestWeigthEdge == null) { // First assignment for this group of vertices
					lowestWeigthEdge = currentLowestWeigthEdge;
				}
				if (currentLowestWeigthEdge == null) { // All the Edges for Vertex already been traversed
					continue;
				}
				if (currentLowestWeigthEdge.getPeso() <= lowestWeigthEdge.getPeso()) {
					lowestWeigthEdge = currentLowestWeigthEdge;
				}
			}
			this.savedEdges.add(lowestWeigthEdge);
			this.visitedVertices.add(lowestWeigthEdge.getVertice1());
			this.visitedVertices.add(lowestWeigthEdge.getVertice2());
		}
		for (Vertice vertexToAdd : this.visitedVertices) {
			newGraph.agregarVertice(vertexToAdd);
		}
		for (Arista edgeToAdd : this.savedEdges) {
			try {
				newGraph.agregarArista(edgeToAdd);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return newGraph;
	}
	
	protected Arista getLowestWeightEdgeWithUnvisitedVertex(HashSet<Arista> edges, Vertice currentVertex) {
		Arista lowestWeightEdge = null;
		int lowestWeight = 0;
		// Assign the first values then break
		for (Arista edge : edges) {
			if (lowestWeightEdge == null 
				&& !this.savedEdges.contains(edge) // Prevents setting as minimum an already used edge.
				&& !(this.visitedVertices.contains(edge.getVertice1()) && this.visitedVertices.contains(edge.getVertice2())) // Prevents a cycle
			) {
				lowestWeightEdge = edge;
				lowestWeight = edge.getPeso();
				break;
			}
		}
		for (Arista edge : edges) {
			if (edge.getPeso() < lowestWeight 
				&& !this.savedEdges.contains(edge)
				&& !(this.visitedVertices.contains(edge.getVertice1()) && this.visitedVertices.contains(edge.getVertice2())) // Prevents a cycle
			) {
				lowestWeightEdge = edge;
				lowestWeight = edge.getPeso();
			}
		}
		return lowestWeightEdge;
	}
}
