package services;

import java.util.ArrayList;

import org.openstreetmap.gui.jmapviewer.Coordinate;

import entities.Agent;
import entities.AgentNetworkVertex;
import entities.Arista;
import entities.Grafo;
import interfaces.MSTreeSolverInterface;
import main.App;

public class AgentManagerService extends Service {
	
	public AgentManagerService(App app) {
		super(app);
	}

	public void registerAgent(Agent agent) {
		ArrayList<Agent> agentStore = this.getAgentRepository();
		agentStore.add(agent);
	}
	
	public void registerAgentLink(Agent agent1, Agent agent2, int distance) throws Exception {
		AgentNetworkVertex agentNetworkVertex = new AgentNetworkVertex(agent1.getName(), agent1);
		AgentNetworkVertex agent2NetworkVertex = new AgentNetworkVertex(agent2.getName(), agent2);
		Arista edge = new Arista(agentNetworkVertex, agent2NetworkVertex, distance);
		this.getAgentLinksRepository().add(edge);
	}
	
	protected ArrayList<Agent> getAgentRepository() {
		@SuppressWarnings("unchecked")
		ArrayList<Agent> agentRepo = (ArrayList<Agent>) this.getAppState().get("agentRepository");
		if (agentRepo == null) {
			agentRepo = this.initAgentsRepository();
		}
		return agentRepo;
	}
	
	protected ArrayList<Arista> getAgentLinksRepository() {
		@SuppressWarnings("unchecked")
		ArrayList<Arista> agentLinksRepo = (ArrayList<Arista>) this.getAppState().get("agentLinkRepository");
		if (agentLinksRepo == null) {
			agentLinksRepo = this.initAgentLinksRepository();
		}
		return agentLinksRepo;
	}
	
	protected Grafo getGrafo() {
		Grafo grafo = (Grafo) this.getAppState().get("grafo");
		if (grafo == null) {
			grafo = this.initAgentNetwork();
		}
		return grafo;
	}

	public Agent getAgentBy(Coordinate coordinate) {
		ArrayList<Agent> agentRepo = this.getAgentRepository();
		for (Agent agent : agentRepo) {
			if (agent.getLat() == coordinate.getLat() && agent.getLon() == coordinate.getLon()) {
				return agent;
			}
		}
		return null;
	}

	public ArrayList<Agent> initAgentsRepository() {
		ArrayList<Agent> agentRepo = new ArrayList<Agent>();
		this.getAppState().set("agentRepository", agentRepo);
		return agentRepo;
	}

	public ArrayList<Arista> initAgentLinksRepository() {
		ArrayList<Arista> agentLinkRepo = new ArrayList<Arista>();
		this.getAppState().set("agentLinkRepository", agentLinkRepo);
		return agentLinkRepo;
	}
	
	public Grafo initAgentNetwork() {
		Grafo grafo = new Grafo();
		this.getAppState().set("grafo", grafo);
		return grafo;
	}
	
	public void resetAgentNetwork() {
		this.initAgentNetwork();
		this.initAgentLinksRepository();
	}

	public Grafo generateAgentNetwork() throws Exception {
		Grafo network = this.getGrafo();
		for (Agent agent : this.getAgentRepository()) {
			AgentNetworkVertex agentNetworkVertex = new AgentNetworkVertex(agent.getName(), agent);
			network.agregarVertice(agentNetworkVertex);
		}
		for (Arista connection : this.getAgentLinksRepository()) {
			network.agregarArista(connection);
		}
		return network;
	}
	
	public Grafo generateAgentNetworkMinimumSpanningTree(MSTreeSolverInterface algorithm, Grafo network) {
		return algorithm.generate(network);
	}
}
