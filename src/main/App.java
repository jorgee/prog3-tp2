package main;

import interfaces.DisplayInterface;
import services.AgentManagerService;

import java.util.HashMap;
import controllers.MainController;

import controllers.Controller;

public class App {
	
	protected AppState appState;
	protected DisplayInterface display;
	protected HashMap<String, Object> classInstancesMap;

	public App(DisplayInterface display) {
		this.display = display;
		this.appState = new AppState();
		this.classInstancesMap = new HashMap<String, Object>();
	}
		
	public void init() throws Exception {
		((MainController) this.getController("Main")).actionMain();
	}
	
	public AppState getAppState() {
		return this.appState;
	}
	
	public DisplayInterface getDisplay() {
		return this.display;
	}
	
	public void setDisplay(DisplayInterface display) {
		this.display = display;
	}
	
	public Controller getController(String name) throws ClassNotFoundException {
		String controllerClass = "controllers." + name + "Controller";
		Controller controllerInstance = (Controller) this.getClassInstance(controllerClass);
		return controllerInstance;
	}
	
	/**
	 * Devuelve una instancia de una clase.
	 * Implementar una especie de Resolver de dependencias es mucho trabajo y lleva tiempo, por lo que simplemente
	 * muevo todas las dependencias a este punto.
	 * Es una especie de container de depedencias muy rústico.
	 * @param className
	 * @return
	 * @throws ClassNotFoundException 
	 */
	protected Object getClassInstance(String className) throws ClassNotFoundException {
		Object classInstance;
		if (this.classInstancesMap.containsKey(className)) {
			classInstance = this.classInstancesMap.get(className);
			return classInstance;
		}
		switch (className) {
			case "controllers.MainController":
				classInstance = new MainController(
					this, 
					(AgentManagerService) this.getClassInstance("services.AgentManagerService")
				);
				break;
			case "services.AgentManagerService":
				classInstance = new AgentManagerService(this);
				break;
			default:
				classInstance = null;
				throw new ClassNotFoundException(
						String.format("Class '%s' is not registered in the Dependency Injection Container.", className)
				);
		}
		this.classInstancesMap.put(className, classInstance);
		return classInstance;
	}
}
