package main;

import java.util.HashMap;

public class AppState {
	
	protected HashMap<String, Object> map;
	
	public AppState() {
		this.map = new HashMap<String, Object>();
	}

	public Object get(String name) {
		return this.map.getOrDefault(name, null);
	}
	
	public void set(String name, Object value) {
		this.map.put(name, value);
	}
	
	public boolean has(String name) {
		return this.map.containsKey(name);
	}
}
