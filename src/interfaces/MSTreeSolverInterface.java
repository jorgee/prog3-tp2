package interfaces;


import entities.Grafo;

public interface MSTreeSolverInterface {
	
	public Grafo generate(Grafo grafo);
}
